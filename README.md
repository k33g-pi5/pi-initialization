# pi-initialization

- SSH Connect into the Pi
- Git clone this repository (`git clone https://gitlab.com/k33g-pi5/pi-initialization.git`)
- Install Docker:
  - Run `install-docker.sh`
- Install Micro editor:
  - Run `install-micro-editor.sh`

